
### Data Files

The theme uses data files for the menu and slider part. For example:

*  To edit the menu, create a **_data/slider.yml** file inside the **source** folder at the root of your project folder.

### Demo

See the [preview demo](https://onweru.github.io/hexo).

### Usage
#### Option 1
Pull this repo into your hexo project themes directory. That is one option; there is a better one,

#### Option 2
You can pull this [example](https://github.com/onweru/example) and edit as you please.

Notice there are 3 scaffold files. Scaffolds help you generate new posts, drafts and pages directly from the command line. When you run the command

> hexo new post hexo-is-kinda-cool

Hexo looks in your scaffolds directory and generate a new file in the **source/_posts/** directory called **hexo-is-kinda-cool.md**. These file contains all the frontmatter values from the **scaffolds/post.md** template, that you specified. 


### Dependencies

This theme uses SASS instead of stylus. For that reason, using this theme requires you to install a sass renderer. Two, it has a search component that requires a json-generator. Use a package manager such as **npm** to  install these dependencies:

> npm install hexo-renderer-sass --save

> npm install hexo-generator-json-content --save

Good luck!

# Post Abstract
If the **summary** value is specified in the front matter, the abstract will be equal to that value. If it's not specified, an excerpt will be displayed instead

### Example

To get an idea of how to use the theme, see the **demo's source code** at this [repo](https://github.com/onweru/example)

### Edit

Feel free to extend the theme to your liking