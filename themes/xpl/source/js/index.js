$(document).ready(function() {

  (function progressBar(){
    $(window).scroll(function() {
      var wintop, docheight, winheight;
      wintop = $(window).scrollTop();
      docheight = $('body').height();
      winheight = $(window).height();
      var totalScroll = (wintop/(docheight-winheight))*100;
      $(".line_scroll").css("width",totalScroll+"%");
    });
  })();
  
  (function animateResizeheader() {
    window.addEventListener('scroll', function(e){
        var distanceY = window.pageYOffset || document.documentElement.scrollTop,
            shrinkOn = 0,
            $header = $(".head");
        if (distanceY > shrinkOn) {
          $header.addClass('resize');
        } else if( $header.hasClass('resize')) {
            $header.removeClass('resize');
        }
    });
  })();
  
  (function myGallery(){
    //Slider
    $("#top-slider").lightSlider({
      // iitem: 1,
      // autoWidth: true,
      // slideMove: 1, // slidemove will be 1 if loop is true
      // slideMargin: 10,

      // addClass: '',
      // mode: "slide",
      // useCSS: false,
      // cssEasing: 'ease', //'cubic-bezier(0.25, 0, 0.25, 1)',//
      // easing: 'linear', //'for jquery animation',////

      // auto: t,
      // loop: true,
      // slideEndAnimation: false,
      // pause: 2000,
      
      gallery: true,
      item: 1,
      mode: 'slide',
      speed: 300, //ms'
      loop: true,
      auto: true,
      slideMargin: 0,
      thumbItem: 3,
      pause: 10000,

      keyPress: true,
      controls: true,
      prevHtml: "<i class='icon-left' aria-hidden='true'></i>",
      nextHtml: "<i class='icon-right' aria-hidden='true'></i>",

      rtl:false,
      adaptiveHeight:false,

      vertical:false,
      verticalHeight:500,
      vThumbWidth:100,

      thumbItem:10,
      pager: true,
      gallery: false,
      galleryMargin: 5,
      thumbMargin: 5,
      currentPagerPosition: 'middle',

      enableTouch:true,
      enableDrag:false,
      freeMove:true,
      swipeThreshold: 40,
    
      onBeforeNextSlide: function() {
        $(this).find('.search-inner').toggleClass('bounceInRight');
      }
    }); 
  })();

  (function siteMenu() {
     $('.toggle-menu').on('click', function() {
       $('.menu-icon').toggleClass('fa-bars').toggleClass('fa-close');
       $('.nav-top').toggleClass('nav-mobile');
       $('.nav-menu').toggleClass('nav-rise');
     });

    $('.special').on('click', function() {
      $(this).find('.sub-menu').toggleClass('expanded');
    });

    $('.special').on('mouseleave', function() {
      $(this).find('.sub-menu').removeClass('expanded');
    });
    
    $( ".sub-menu" ).on('mouseleave', function() {
      $(this).removeClass('expanded');
    });

  })();

  $('.article img').on('click', function() {
    if ($(this).parents('.article-zoom').length == 0) {
      $(this).wrap("<div class='article-zoom'></div>");
      $('.article-zoom').on('click', function(e) {
        if(e.target === this ) {
          $(this).find('img').unwrap();
        }
      });
    }
  });



});
